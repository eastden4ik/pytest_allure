import allure
import pytest


@allure.feature('HTTP Cat statuses')
@allure.story('Получение фото статусов с котами')
@pytest.mark.parametrize("status", [
    "200",
    "201",
    "204",
    "404",
    "403",
    "401",
    "503",
    "60000",
    "434345",
    "343"
])
def test_get_random_dog(cat_http_api, status):
    response = cat_http_api.get(f'{status}')

    with allure.step("Запрос отправлен, посмотрим код ответа"):
        assert response.status_code == 200, f"Неверный код ответа, получен {response.status_code}"
