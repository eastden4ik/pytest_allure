import allure
import pytest
import requests


class ApiClient:

    def __init__(self, base: str):
        self.base_url = base

    def post(self,
             path: str = None,
             params: str = None,
             data: str = None,
             json: str = None,
             headers: str = None):
        url = f'{self.base_url}{path}'
        with allure.step(f'POST request to: {url}'):
            return requests.post(url=url,
                                 params=params,
                                 data=data,
                                 json=json,
                                 headers=headers)

    def get(self,
            path: str = None,
            params: str = None,
            headers: str = None):
        url = f'{self.base_url}{path}'
        with allure.step(f'GET request to: {url}'):
            return requests.get(url=url,
                                params=params,
                                headers=headers)


@pytest.fixture
def dog_api():
    return ApiClient(base="https://dog.ceo/api/")


@pytest.fixture
def cat_http_api():
    return ApiClient(base="https://http.cat/")
